const express = require('express');
const chai = require('chai');
const request = require('supertest');
const { expect } = require('chai');

const app = express();

describe('POST should add an artwork', () => {
    it('should create a new artwork', () => {
        request(app)
        .post('/gallery')
        .send({})
        .expect(201)
        .then((res) => {
            expect(res.headers.location).to.be.eql('/gallery');
        });
    });
});

