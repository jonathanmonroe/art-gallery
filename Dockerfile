 #!/usr/bin/env bash
FROM node:16.5.0

ENV NODE_ENV=production

WORKDIR /app

COPY ["package.json", "package-lock.json*", "./"]

RUN npm install --production

COPY . .

ENTRYPOINT ["node", "app.js"]