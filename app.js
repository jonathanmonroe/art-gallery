const Express = require("express");
const BodyParser = require("body-parser");
const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectId;
const { response } = require("express");
const DATABASE_NAME = "artworkDB";
const CONNECTION_URL = "mongodb+srv://mongo-admin:admin@cluster0.iqij9.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

var app = Express();
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));

app.post("/gallery", (req, res) =>{
    collection.insertOne(req.body, (error, result) => {
        if(error){
            return res.status(500).send(error);
        }
        res.send(result.result);
    });
});

app.get("/gallery", (req, res) => {
    collection.find({}).toArray((error, result) => {
        if (error)  {
            return res.status(500).send(error);
        }
        res.send(result)
    });
});

app.delete("/gallery/:id", (req, res) => {
    collection.deleteOne({"_id": new ObjectId(req.params.id)}, (error, result) => {
        if (error) {
            return res.status(500).send(error)
        }
        res.send(result)
    })
})

app.get("/gallery/:id", (req, res) => {
    collection.findOne({"_id": new ObjectId(req.params.id)}, (error, result) =>{
        if (error) {
            return res.status(500).send(error);
        }
        res.send(result);
    });
});

app.listen(3000, () => {
    MongoClient.connect(CONNECTION_URL, { useNewUrlParser: true }, (error, client) => {
        if(error) {
            throw error;
        }
        database = client.db(DATABASE_NAME);
        collection = database.collection("artwork");
        console.log("Connected to `" + DATABASE_NAME + "`!");
    });
});